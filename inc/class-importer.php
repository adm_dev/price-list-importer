<?php

namespace ADM\PriceListImporter;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Importer
{
    public $success = [];
    public $errors = [];
    public $delimiter = ';';
    public $allowed_product_types = ['simple', 'variable-subscription', 'subscription_variation', 'subscription'];
    public $dry_run = false;
    public $update_price = false;
    public $update_sale_price = false;
    public $home_url = '';

    /**
     * Importer constructor.
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * Initialize the importer.
     */
    public function init()
    {
        // Add admin styles
        add_action('admin_head', [$this, 'admin_styles']);

        $this->home_url = get_admin_url(null, 'admin.php?page=import-prices');
    }

    /**
     * Get current site name.
     *
     * @return string
     */
    public function get_site_name()
    {
        if (is_multisite()) {
            $blog_details = get_blog_details(get_current_blog_id());
            return $blog_details->blogname;
        } else {
            return get_bloginfo('name');
        }
    }

    /**
     * Render the front-end.
     *
     * @return mixed|void
     */
    public function render()
    {
        ?>
        <div class="wrap">
            <h1 class="wp-heading-inline"><?= __('Import Price List', 'price-importer') ?></h1>

            <?php
            $step = isset($_GET['step']) ? $_GET['step'] : 1;

            switch (intval($step)) {
                case 2:
                    $this->import_results();
                    echo '<p><a href="' . $this->home_url . '">' . __(
                            'Go back to Product Import page',
                            'price-importer'
                        ) . '</a></p>';
                    break;

                default:
                    $this->upload_form();
            }
            ?>
        </div>
        <?php
    }

    /**
     * Render the upload form.
     */
    public function upload_form()
    {
        $bytes = apply_filters('import_upload_size_limit', wp_max_upload_size());
        $size = size_format($bytes);
        ?>

        <?php if (is_multisite()): ?>
        <p>
            <?=
            sprintf(
                __('This tool will import and update the prices of variation and simple products only in this sub-site (%s).', 'price-importer'),
                '<strong>' . $this->get_site_name() . '</strong>'
            ) ?>
        </p>
        <?php endif ?>

        <p>
            <?= __('CSV file should have the following columns: SKU, regular price, sale price.', 'price-importer') ?>
            <?= __('First row of the CSV file is ignored (headers).', 'price-importer') ?>
        </p>

        <form enctype="multipart/form-data" id="import-product-prices" method="post"
              action="<?= $this->home_url ?>&step=2">

            <?php wp_nonce_field('product_price_import_nonce', 'product_price_import_nonce') ?>

            <table class="form-table">
                <tbody>
                <tr>
                    <th>
                        <label for="upload"><?php esc_html_e(
                                'Choose a file from your computer:',
                                'price-importer'
                            ); ?></label>
                    </th>
                    <td>
                        <input type="file" id="upload" name="import" size="25"/>
                        <input type="hidden" name="action" value="save"/>
                        <input type="hidden" name="max_file_size" value="<?php echo absint($bytes); ?>"/>
                        <small>
                            <?php
                            printf(
                                esc_html__('Maximum size: %s', 'price-importer'),
                                esc_attr($size)
                            );
                            ?>
                        </small>
                    </td>
                </tr>
                <tr>
                    <th><label><?php esc_html_e('Delimiter', 'price-importer'); ?></label><br/></th>
                    <td><input type="text" name="delimiter" placeholder=";" size="2"/></td>
                </tr>
                <tr>
                    <th><label><?php esc_html_e('What would you like to update?', 'price-importer'); ?></label><br/></th>
                    <td>
                        <p>
                            <input type="checkbox" name="update_price" id="update_price"/>
                            <label for="update_price"><?php esc_html_e('Regular price', 'price-importer'); ?></label>
                        </p>

                        <p>
                            <input type="checkbox" name="update_sale_price" id="update_sale_price"/>
                            <label for="update_sale_price"><?php esc_html_e('Sale price', 'price-importer'); ?></label>
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
            <p class="submit">
                <?php submit_button('Upload file and dry run', 'secondary', 'upload-and-dry-run', false); ?>
                <?php submit_button('Upload file and import', 'primary', 'upload-and-import', false); ?>
            </p>
        </form>

        <script>
            jQuery('input[name=upload-and-import]').click(function (e) {
                e.preventDefault();
                if (confirm('<?= __(
                    'Are you sure you want to run the importer? This action will update all prices listed in the CSV file and it is irreversible.',
                    'price-importer'
                ) ?>')) {
                    jQuery('#import-product-prices').submit();
                }
            });
        </script>
        <?php
    }

    /**
     * Execute import and generate results page.
     *
     * @return bool
     */
    public function import_results()
    {
        if (!wp_verify_nonce($_POST['product_price_import_nonce'], 'product_price_import_nonce')) {
            wp_redirect($this->home_url);
            exit;
        }

        if (isset($_POST['upload-and-dry-run'])) {
            $this->dry_run = true;
        }

        $file_id = $this->handle_upload();

        if (!$file_id) {
            $this->show_errors();
            return false;
        }

        $file = get_attached_file($file_id);

        add_filter('http_request_timeout', [$this, 'bump_request_timeout']);

        if (isset($_POST['delimiter']) && !empty($_POST['delimiter'])) {
            $this->delimiter = $_POST['delimiter'];
        }

        $this->update_price = isset($_POST['update_price']);
        $this->update_sale_price = isset($_POST['update_sale_price']);

        // File upload is complete, import the prices
        $this->handle_import($file);

        // Importer is finished, delete the file
        wp_delete_attachment($file_id, true);

        if (!empty($this->errors)) {
            $this->show_errors();
        } else {
            $this->show_success();
        }

        return true;
    }

    /**
     * Handle file upload.
     *
     * @return bool|int
     */
    public function handle_upload()
    {
        if (!isset($_POST['update_price']) && !isset($_POST['update_sale_price'])) {
            $this->error(__('Please select what data you would like to update.', 'price-importer'));
            return false;
        }

        $file = wp_import_handle_upload();

        if (isset($file['error'])) {
            $this->error($file['error']);
            return false;
        }

        if (!wc_is_file_valid_csv($file['file'], false)) {
            wp_delete_attachment($file['id'], true);

            $this->error(__('Corrupt file or invalid file type.', 'price-importer'));
            return false;
        }

        return absint($file['id']);
    }

    /**
     * Parse file data and handle import.
     *
     * @param $file
     * @return bool
     */
    public function handle_import($file)
    {
        if (!is_file($file)) {
            $this->error(__('Invalid file.', 'price-importer'));
            return false;
        }

        $handle = fopen($file, 'r');

        if ($handle === false) {
            $this->error(__('Unable to open uploaded file.', 'price-importer'));
            return false;
        }

        $current_row = 0;

        $output = [];

        while (($row = fgetcsv($handle, 1000, $this->delimiter)) !== false) {
            $current_row++;

            if ($current_row == 1 && count($row) < 3) {
                $this->error(__('Invalid CSV format. Please check if all the columns are correct.', 'price-importer'));
                fclose($handle);
                return false;
            }

            $output[$current_row] = $this->parse_row($row, $current_row);
        }

        $this->success = $output;

        fclose($handle);

        return true;
    }

    /**
     * Parse one single row from a spreadsheet file and generate array with result data.
     *
     * @param $data
     * @param $current_row
     * @return array
     */
    public function parse_row($data, $current_row)
    {
        $result = [
            'status' => 'fail',
            'message' => [],
            'sku' => '-',
        ];

        if ($current_row <= 1) {
            $result['status'] = 'skip';
            $result['message'][] = __('Skipping CSV headers', 'price-importer');
            return $result;
        }

        list($product_sku, $product_price, $product_sale_price) = array_map(
            'trim',
            $data
        );

        if (empty($product_sku)) {
            $result['message'][] = __('Missing product SKU', 'price-importer');
            return $result;
        }

        $result['sku'] = $product_sku;

        $product_id = wc_get_product_id_by_sku($product_sku);

        if (!$product_id) {
            $result['message'][] = __('Product not found with this SKU', 'price-importer');
            return $result;
        }

        $product = wc_get_product($product_id);

        if (!$product) {
            $result['message'][] = __('Product not found with this SKU', 'price-importer');
            return $result;
        }

        $product_real_type = $product->get_type();

        if (!in_array($product_real_type, $this->allowed_product_types)) {
            $result['message'][] = __('Invalid product type', 'price-importer');
            return $result;
        }

        $skipping = true;

        // Update price
        if ($this->update_price) {
            $product_old_price = $product->get_regular_price();

            $product_price = wc_format_decimal($product_price);

            if ($product_old_price == $product_price) {
                $result['message'][] = __('Skipping regular price update - no change', 'price-importer');
            } else {
                $failed = false;

                if (!$this->dry_run) {
                    try {
                        $product->set_regular_price($product_price);
                        $product->save();
                    } catch (\Exception $e) {
                        $failed = true;
                        $result['status'] = 'fail';
                        $result['message'][] = __('Error occurred when updating price: ', 'price-importer') . $e->getMessage();
                    }
                }

                if (!$failed) {
                    $skipping = false;
                    $result['status'] = 'success';
                    $result['message'][] = __('Successfully updated regular price. ', 'price-importer') . $this->str_before_after(
                            $product_old_price,
                            $product_price,
                            'price'
                        );
                }
            }
        }

        // Update sale price
        if ($this->update_sale_price) {
            $product_old_sale_price = $product->get_sale_price();

            $product_regular_price = $product->get_regular_price();
            $product_sale_price = wc_format_decimal($product_sale_price);

            if ($product_old_sale_price == $product_sale_price) {
                $result['message'][] = __('Skipping sale price update - no change', 'price-importer');
            } elseif ($product_regular_price < $product_sale_price) {
                $skipping = false;
                $result['message'][] = sprintf(
                        __('Couldn\'t set sale price. Sale price (%s) is higher than regular price (%s).', 'price-importer'),
                        $product_sale_price,
                        $product_regular_price
                );
            } else {
                $failed = false;

                if (!$this->dry_run) {
                    try {
                        $product->set_sale_price($product_sale_price);
                        $product->save();
                    } catch (\Exception $e) {
                        $failed = true;
                        $result['status'] = 'fail';
                        $result['message'][] = __('Error occurred when updating sale price: ', 'price-importer') . $e->getMessage();
                    }
                }
                if (!$failed) {
                    $skipping = false;
                    $result['status'] = 'success';
                    $result['message'][] = __('Successfully updated sale price. ', 'price-importer') . $this->str_before_after(
                            $product_old_sale_price,
                            $product_sale_price,
                            'sale price'
                        );
                }
            }
        }

        if ($skipping) {
            $result['status'] = 'skip';
        }

        return $result;
    }

    /**
     * Show report after successful import.
     */
    public function show_success()
    {
        $rows = $this->success;
        ?>

        <?php if ($this->dry_run): ?>
            <h3>
                <?= __('This was a DRY RUN which means that none of the data shown below was actually changed.', 'price-importer') ?>
            </h3>
        <?php endif; ?>

        <p>
            <strong><?= sprintf(__('Succesfully processed %s rows.', 'price-importer'), count($rows)) ?></strong>
        </p>

        <table class="wc_status_table widefat">
            <thead>
            <tr>
                <th><?= __('Row #', 'price-importer') ?></th>
                <th><?= __('Product', 'price-importer') ?></th>
                <th><?= __('Result', 'price-importer') ?></th>
                <th><?= __('Status', 'price-importer') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($rows as $index => $row): ?>
                <tr class="status-<?= esc_attr($row['status']) ?>">
                    <td><?= $index ?>.</td>
                    <td><?= $row['sku'] ?></td>
                    <td>
                        <?= implode('<br>', $row['message']) ?>
                    </td>
                    <td><?= $row['status'] ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <?php
    }

    /**
     * Set error.
     *
     * @param $msg
     */
    public function error($msg)
    {
        $this->errors[] = $msg;
    }

    /**
     * Show errors.
     */
    public function show_errors()
    {
        echo '<div class="errors">';
        foreach ($this->errors as $error) {
            echo '<p>' . $error . '</p>';
        }
        echo '</div>';
    }

    /**
     * Added to http_request_timeout filter to force timeout at 60 seconds during import.
     *
     * @param $val
     * @return int
     */
    public function bump_request_timeout($val)
    {
        return 60;
    }

    /**
     * Admin styles for import report.
     *
     * @return void|bool
     */
    public function admin_styles()
    {
        if (!isset($_GET['page']) || $_GET['page'] != 'import-prices') {
            return false;
        }
        ?>
        <style>
            table.wc_status_table th {
                font-weight: bold;
            }

            table.wc_status_table td:first-child {
                width: auto;
            }

            table.wc_status_table tbody td:last-child {
                font-weight: bold;
                text-transform: uppercase;
            }

            table.wc_status_table tr.status-success td:last-child {
                color: green;
            }

            table.wc_status_table tr.status-fail td:last-child {
                color: red;
            }

            table.wc_status_table tr.status-skip td:last-child {
                color: gray;
            }
        </style>
        <?php
    }

    /**
     * Format "before and after" string.
     *
     * @param $before
     * @param $after
     * @param string $object
     * @return string
     */
    public function str_before_after($before, $after, $object = '')
    {
        $before = trim($before);
        $after = trim($after);
        $before = empty($before) ? '-' : $before;
        $after = empty($after) ? '-' : $after;

        return sprintf(
            __('%s before: %s, %s after: %s', 'price-importer'),
            ucfirst($object),
            '<strong>' . $before . '</strong>',
            $object,
            '<strong>' . $after . '</strong>'
        );
    }
}