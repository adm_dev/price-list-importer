<?php
/*
Plugin Name: Price List Importer
Plugin URI: https://www.adm.ee
Description: Price list importer for WooCommerce.
Author: ADM Interactive
Version: 1.0
Author URI: http://www.adm.ee
*/

namespace ADM\PriceListImporter {

    class PriceListImporter {
        public $importer;

        /**
         * PriceListImporter constructor.
         */
        public function __construct()
        {
            $this->init();
        }

        /**
         * Initialize the plugin.
         */
        public function init()
        {
            $pluginPath = plugin_dir_path(__FILE__);
            require_once($pluginPath . '/inc/class-importer.php');

            $this->importer = new Importer();
            add_action('admin_menu', [$this, 'admin_menu']);
        }

        /**
         * Add menu item.
         */
        public function admin_menu()
        {
            add_menu_page(__('Price List Importer', 'price-importer'), __('Price List Importer', 'price-importer'), 'publish_posts', 'import-prices', [$this->importer, 'render'], 'dashicons-upload', 26);
        }
    }
}

namespace {
    new \ADM\PriceListImporter\PriceListImporter();
}