# Price List Importer #

This is a basic **WooCommerce** plugin that enables you to import product prices from a CSV file.

### How does it work? ###

* This plugin creates a new admin menu item "Price List Importer". Price lists can be uploaded on that page.
* Importer accepts only valid CSV files and expects there to be at least 3 data columns (SKU, regular price, sale price)
* Currently only regular price and sale price can be updated. If necessary, more features can be easily implemented.
* Importer offers **"dry run"** mode which runs the importer as it would normally, except it doesn't actually change 
any data.
* After the importer is finished a detailed report screen will be shown with statuses and values what were changed.
* Plugin includes an example CSV file.

### Developer notes ###

* This is a basic plugin, but it's a good example/base for any importer-type plugin. 
* Feel free to extend this plugin's functionality or use it to create something else.

Original idea for this plugin came from Care Club's price updater and Confido's price list importer.